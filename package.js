Package.describe({
    name: 'molosc:wavesurfer-regions',
    version: '0.0.3',
    // Brief, one-line summary of the package.
    summary: 'Regions Plugin for Wavesurfer',
    // URL to the Git repository containing the source code for this package.
    git: 'https://Molosc@bitbucket.org/Molosc/wavesurfer-regions-plugin.git',
    // By default, Meteor will default to using README.md for documentation.
    // To avoid submitting documentation, set this field to null.
    documentation: 'README.md'
});

Package.onUse(function(api) {
    api.versionsFrom('1.2.1');
    api.use('ecmascript');
    api.use('jj227:wavesurfer@0.5.0');
    api.addFiles(['wavesurfer-regions.js'], 'client');
});

Package.onTest(function(api) {
    api.use('ecmascript');
    api.use('tinytest');
    api.use('molosc:wavesurfer-regions');
    api.addFiles('wavesurfer-regions-tests.js');
});